package com.example.wallmartexample.views.base

interface BaseView<T> {

    var presenter: T?
}