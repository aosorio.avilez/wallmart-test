package com.example.wallmartexample.views.base

interface BasePresenter {
    fun bind()

    fun unbind()
}