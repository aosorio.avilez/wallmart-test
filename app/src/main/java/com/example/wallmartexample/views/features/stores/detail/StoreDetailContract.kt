package com.example.wallmartexample.views.features.stores.detail

import com.example.wallmartexample.data.models.Store
import com.example.wallmartexample.views.base.BasePresenter
import com.example.wallmartexample.views.base.BaseView

interface StoreDetailContract {

    interface View: BaseView<Presenter> {

        fun setupView()

        fun showStore(store: Store)
    }

    interface Presenter: BasePresenter {
        fun onShowStore()
    }
}