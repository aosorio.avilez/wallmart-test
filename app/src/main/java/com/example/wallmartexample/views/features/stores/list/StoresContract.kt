package com.example.wallmartexample.views.features.stores.list

import com.example.wallmartexample.data.models.Store
import com.example.wallmartexample.views.base.BasePresenter
import com.example.wallmartexample.views.base.BaseView

interface StoresContract {

    interface View: BaseView<Presenter> {

        fun setupView()

        fun showStores(stores: List<Store>)

        fun showStoreDetailView(store: Store)

        fun showProgress()

        fun hideProgress()
    }

    interface Presenter: BasePresenter {
        fun fetchStores()

        fun onStoreClicked(position: Int)
    }
}