package com.example.wallmartexample.views.features.stores.list

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.wallmartexample.R
import com.example.wallmartexample.data.models.Store
import com.example.wallmartexample.data.remote.services.StoreService
import com.example.wallmartexample.views.features.stores.detail.StoreDetailActivity
import kotlinx.android.synthetic.main.progress_bar.*
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback

import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions


class StoresActivity : AppCompatActivity(),
    StoresContract.View, OnMapReadyCallback {

    override var presenter: StoresContract.Presenter? = null

    private var map: GoogleMap? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        presenter = StoresPresenter(
            this,
            StoreService()
        )

        presenter?.bind()
    }

    override fun onDestroy() {
        presenter?.unbind()
        super.onDestroy()
    }

    override fun setupView() {
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment!!.getMapAsync(this)
    }

    override fun showStores(stores: List<Store>) {
        print(stores)
        map?.clear()

       for (store in stores) {
           val marker = MarkerOptions()
           marker.position(LatLng(store.latPoint.toDouble(), store.lonPoint.toDouble()))
           marker.title(store.name)

           map?.addMarker(marker)
       }
    }

    override fun onMapReady(p0: GoogleMap?) {
        map = p0
        map?.setOnMarkerClickListener {
            presenter?.onStoreClicked(1)
            true
        }
        presenter?.fetchStores()
    }

    override fun showStoreDetailView(store: Store) {
        val intent = Intent(this, StoreDetailActivity::class.java)
        intent.putExtra("store", store)
        startActivity(intent)
    }

    override fun showProgress() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progressBar.visibility = View.GONE
    }
}
