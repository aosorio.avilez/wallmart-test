package com.example.wallmartexample.views.features.stores.detail

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.wallmartexample.R
import com.example.wallmartexample.data.models.Store
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.activity_store_detail.*

class StoreDetailActivity : AppCompatActivity(), StoreDetailContract.View, OnMapReadyCallback {

    override var presenter: StoreDetailContract.Presenter? = null

    private var map: GoogleMap? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_store_detail)

        intent.getParcelableExtra<Store>("store")?.let {
            presenter = StoreDetailPresenter(this, it)

            presenter?.bind()
        }
    }

    override fun onDestroy() {
        presenter?.unbind()
        super.onDestroy()
    }

    override fun onMapReady(p0: GoogleMap?) {
        map = p0
        presenter?.onShowStore()
    }

    override fun setupView() {
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment!!.getMapAsync(this)
    }

    override fun showStore(store: Store) {
        map?.clear()
        val marker = MarkerOptions()
        marker.position(LatLng(store.latPoint.toDouble(), store.lonPoint.toDouble()))
        marker.title(store.name)
        map?.addMarker(marker)

        map?.animateCamera(CameraUpdateFactory.newCameraPosition(CameraPosition.fromLatLngZoom(LatLng(store.latPoint.toDouble(), store.lonPoint.toDouble()), 7F)))

        storeNameLabel.text = store.name
        addressLabel.text = store.address
        scheduleLabel.text = store.opens
    }
}
