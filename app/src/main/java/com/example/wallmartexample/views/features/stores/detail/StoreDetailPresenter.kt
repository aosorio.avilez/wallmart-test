package com.example.wallmartexample.views.features.stores.detail

import com.example.wallmartexample.data.models.Store

class StoreDetailPresenter(private var view: StoreDetailContract.View?, private val store: Store): StoreDetailContract.Presenter {

    override fun unbind() {
        view = null
    }

    override fun bind() {
        view?.setupView()
    }

    override fun onShowStore() {
        view?.showStore(store)
    }
}