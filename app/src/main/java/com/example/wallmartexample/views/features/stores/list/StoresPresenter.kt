package com.example.wallmartexample.views.features.stores.list

import com.example.wallmartexample.data.models.Store
import com.example.wallmartexample.data.remote.services.StoreService

class StoresPresenter(private var view: StoresContract.View?,
                      private var service: StoreService?):
    StoresContract.Presenter {

    private var stores: List<Store> = emptyList()

    override fun onStoreClicked(position: Int) {
        view?.showStoreDetailView(stores[position])
    }

    override fun fetchStores() {
        view?.showProgress()

        service?.stores({
            view?.hideProgress()
            this.stores = it
            view?.showStores(it)
        }, { _, _ ->
            view?.hideProgress()
        })
    }

    override fun bind() {
        view?.setupView()
    }

    override fun unbind() {
        service = null
        view = null
    }
}