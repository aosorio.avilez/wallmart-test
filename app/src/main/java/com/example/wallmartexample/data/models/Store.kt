package com.example.wallmartexample.data.models

import android.os.Parcel
import android.os.Parcelable

data class Store(
    val businessID: String,
    val storeID: String,
    val name: String,
    val address: String,
    val telephone: String,
    val manager: String,
    val zipCode: String,
    val latSpan: String,
    val latPoint: String,
    val lonPoint: String,
    val opens: String
): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(businessID)
        parcel.writeString(storeID)
        parcel.writeString(name)
        parcel.writeString(address)
        parcel.writeString(telephone)
        parcel.writeString(manager)
        parcel.writeString(zipCode)
        parcel.writeString(latSpan)
        parcel.writeString(latPoint)
        parcel.writeString(lonPoint)
        parcel.writeString(opens)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Store> {
        override fun createFromParcel(parcel: Parcel): Store {
            return Store(parcel)
        }

        override fun newArray(size: Int): Array<Store?> {
            return arrayOfNulls(size)
        }
    }
}