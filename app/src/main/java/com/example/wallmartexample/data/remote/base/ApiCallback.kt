package com.example.wallmartexample.data.remote.base

import retrofit2.Call
import retrofit2.Response

class ApiCallback<T>(private val callback: (Int, ApiResponse<T>?, Throwable?) -> Unit): retrofit2.Callback<ApiResponse<T>> {

    override fun onResponse(call: Call<ApiResponse<T>>, response: Response<ApiResponse<T>>) {
        if (response.isSuccessful) {
            callback(200, response.body(), null)
        } else {
            callback(response.code(), null, Throwable(response.message()))
        }
    }

    override fun onFailure(call: Call<ApiResponse<T>>, t: Throwable) {
        callback(500, null, t)
    }
}