package com.example.wallmartexample.data.remote.base

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

private const val baseUrl = "https://www.walmartmobile.com.mx/walmart-services/"

open class BaseService {
    private var retrofit: Retrofit? = null

    fun <S> getEndpoint(serviceClass: Class<S>): S {
        if (retrofit == null) {
            val httpClientBuilder = OkHttpClient.Builder()

            httpClientBuilder.build()

            val gson = GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                .setLenient()
                .create()

            retrofit = Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(httpClientBuilder.build())
                .build()
        }
        return retrofit!!.create(serviceClass)
    }
}