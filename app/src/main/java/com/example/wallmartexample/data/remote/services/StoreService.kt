package com.example.wallmartexample.data.remote.services

import com.example.wallmartexample.data.models.Store
import com.example.wallmartexample.data.remote.base.ApiCallback
import com.example.wallmartexample.data.remote.base.ApiResponse
import com.example.wallmartexample.data.remote.base.BaseService
import retrofit2.Call
import retrofit2.http.GET

class StoreService: BaseService() {

    interface StoreEndpoints {

        @GET("mg/address/storeLocatorCoordinates")
        fun stores(): Call<ApiResponse<List<Store>>>
    }

    fun stores(callback: (List<Store>) -> Unit, errorCallback: (Int, Throwable) -> Unit) {
        val endpoint = getEndpoint(StoreEndpoints::class.java)
        endpoint.stores().enqueue(ApiCallback { status, data, error ->
            if (error != null) {
                errorCallback(status, error)
                return@ApiCallback
            }

            data?.let { callback(it.data) }
        })
    }
}