package com.example.wallmartexample.data.remote.base

import com.google.gson.annotations.SerializedName

data class ApiResponse<T>(
    val codeMessage: Int,
    val message: String,
    @SerializedName("responseArray")
    val data: T)